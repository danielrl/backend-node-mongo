/*=============================
REQUIRIENDO NPM INSTALL
===============================*/

const routerx = require('express-promise-router');
const router = routerx();

/*=============================
REQUIRIENDO CONTROLADOR GENERO ML
===============================*/

const generoController = require('../controllers/genero');

/*=============================
RUTAS CON METODOS DEL CONTROLADOR
===============================*/

router.post('/registrar', generoController.crearGenero);
router.get('/listar-genero', generoController.listarGenero);


/*============================
EXPORTANDO EL VALOR DE ROUTER
==============================*/

module.exports = router;