/*==============================
REQUIRIENDO INSTALL NPM
================================*/

const routerx = require('express-promise-router');
const router = routerx();

/*==============================
REQUIRIENDO TODAS LAS RUTAS
================================*/

const generoRouters = require('./genero');


/*===============================
USANDO TODAS LAS RUTAS
=================================*/

router.use('/genero', generoRouters);


/*===============================
EXPORTANDO VALOR DE ROUTER
=================================*/

module.exports = router;