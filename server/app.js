/*============================
REQUIRIENDO INSTALL NPM
==============================*/

const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const routers = require('./routers/index');

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


/*==============================
HABILITANDO PUERTO
================================*/

app.set('port', process.env.PORT || 8000);

/*==============================
USO DE MIDDLEWARES
================================*/

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


/*===============================
ADJUNTANDO LAS RUTAS
=================================*/

app.use('/api', routers);


/*==============================
EXPORTANDO EL VALOR APP
================================*/

module.exports = app
