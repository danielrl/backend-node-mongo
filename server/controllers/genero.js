/*===============================
REQUIRIENDO EL MODEL GENERO
=================================*/

const ctrGenero = {};

const mdlGenero = require('../models/genero');

ctrGenero.crearGenero = async (req, res, next) => {

    try {

        const respuesta = await mdlGenero.create(req.body);
        res.status(200).json(respuesta);
        
    } catch (e) {
        res.status(500).json({ok: false, mensaje: 'ERROR AL REGISTRAR'});
        next(e);
    }

}

ctrGenero.listarGenero = async (req, res, next) => {

    try {

        const respuesta = await mdlGenero.find();
        res.status(200).json({ok: true, data: respuesta});
        
    } catch (e) {
        res.status(500).json({ok: false, mensaje: 'Error AL LISTAR'});
        next(e);
    }

}

/*==============================
EXPORTANDO EL VALOR DE CTRGENERO
================================*/

module.exports = ctrGenero;