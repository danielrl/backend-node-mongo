/*===========================
REQUIRIENDO LA CONEXION
=============================*/

require('dotenv').config();
require('./config/config');

const app = require('./app');
require('./database');




async function server() {
    await app.listen(app.get('port'));
    console.log(`Escuchando puerto ${app.get('port')}`);
}

server();