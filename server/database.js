/*===========================
REQUIRIENDO INSTALL NPM
=============================*/

const mongoose = require('mongoose');

/*============================
DIRECCION DE DB
==============================*/

const URI = process.env.MONGODB_IRU ?
process.env.MONGODB_IRU : 'mongodb://localhost:27017/db_prueba';


/*=============================
CONEXION A MONGODB POR MONGOOSE
===============================*/

mongoose.connect(URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
})

/*===============================
COMPROBANDO CONEXION
=================================*/

const connection = mongoose.connection;
connection.once('open', () => {
    console.log(`conectado a la DB`);
})

